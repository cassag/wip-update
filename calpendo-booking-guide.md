# WIN Works in Progress (WIP) Calpendo Booking Procedure

*This guide is intended to be made available for WIN researchers via the intranet.*

**Outline**

1. [What is Calpendo?](#what-is-calpendo)
2. [How do I get a Calpendo account?](#how-do-i-get-a-calpendo-account?)
3. [For WIP Presenters](#for-wip-presenters)
4. [For WIN Wednesday Coordinators](#for-win-wednesday-coordinators)
5. [For Administrators](#for-administrators)


## What is Calpendo?

Calpendo is a customisable "facilities management system". We use Calpendo at WIN to book scanning time, research labs, testing rooms, meeting rooms, research equipment, and even bicycle parking spaces! Calpendo is well suited for these tasks as we can closely define rules for the booking and control access. Some resources are available for everyone, others can only be booked by people who have completed the required training.

We use Calpendo to book WIN Wednesday presentations as it minimises the friction and email traffic in making your booking - you can instantly see what slots are available and edit your booking directly. Calpendo can also collect the required documentation (for example signed recording releases) and send automated reminders to help you prepare for your presentation.

## How do I get a Calpendo account?

Calpendo is available to all WIN members, but you need an account to access it.

To create your Calpendo account, go to http://calpendo.fmrib.ox.ac.uk/ and "sign in using Shibboleth". Enter your Shibboleth ("SSO") username and password.

![calpendo sign in page with arrow highlighting Shibboleth sign in button](img/calpendo-shibboleth.png)

If your Shibboleth account has not already been granted access to calpendo, you will be invited to complete an account request form. This form will be sent to the administrator of your department to verify you as a user. It is important to correctly identify your Department so the form is directed appropriately.

**Creating accounts requires the manual intervention of WIN Administrators. This means it is not instantaneous. Please allow 1 week for your account to be issued.** Please contact sebastian.rieger@psych.ox.ac.uk to follow up if your account has not been created in \> 1 week following your request.

Accounts may be suspended if there is no Calpendo activity for \> 1 year. If your account has been suspended, please contact sebastian.rieger@psych.ox.ac.uk to request it is reinstated.


## For WIP Presenters

Please see [this guide](WIP-guide.md) for a description of what to expect from a WIP presentation, what to include, and where to get support.

The following describes the process for booking a WIP presentation using calpendo.  

### How to book a WIP

WIP presentations are advertised in the WIN Monday Message the week of your presentation. **It is therefore advisable to book your presentation by no later than 12:00 on Friday of the week proceeding your slot.**

#### 1. Create an account

Ensure you have an active Calpendo account. This may take up to one week to review or reinstate. See [How do i get a Calpendo account](#how-do-i-get-a-calpendo-account).

#### 2. Log into Calpendo using Shibboleth

Go to http://calpendo.fmrib.ox.ac.uk/.

Click the "Shibboleth" button indicated below.

On the following page use your University of Oxford Shibboleth SSO to sign into the Calpendo homepage.

![calpendo sign in page with arrow highlighting Shibboleth sign in button](img/calpendo-shibboleth.png)

#### 3. Select the WIN Wednesday resource

Select the WIN Wednesday resource from the list of available calendars.

Click the "Calendars" tab on the top left of the Calpendo page, then select WIN Wednesdays as shown below.

![calpendo booking calendar with arrows highlighting the calendar drop down and WIN Wednesday resource](img/calpendo-resource.png)

#### 4. Find an available slot

Use the week navigation buttons near the top of the page to search through the calendar and find an available date for your presentation. Bookings are not allowed in the red shaded areas.

![calpendo booking calendar for WIN Wednesdays with arrows highlighting the week navigation buttons and an available slot](img/calpendo-available.png)

For any given week, there may be no pre-booked presentations, or there may be a seminar already scheduled. If there is a white space in the calendar, you are welcome to book it. This may mean that your presentation is held before or after another presentation.

#### 5. Click to open the "new booking" window

Click in an available slot in the calendar to open the "new booking" window. Enter the required details as described below.

![calpendo new booking window for WIN Wednesdays with arrows highlighting: 1) end time; 2) email reminder notice period; 3) description text box; 4) Title, Authors and Abstract text box; 5) Recording consent upload button](img/calpendo-newbooking.png)

##### 1. Start and end time

As described in the [WIP Guide](WIP-guide.md), WIP presentations are 10 minutes long = 8 minutes presentation + 2 minutes questions.

Ensure the start time of your WIP is 12:00. Adjust the end time to 12:10.

**Please note that the exact start time of each presentation within a WIN Wednesday session is subject to change, and we therefore request that speakers are present for the duration 12:00-13:00 the session.**

**We request that all presenters join the call 10 minutes before the start of the meeting to test audio-visual equipment with the host.**

##### 2. Reminder notice period

This sets the date which Calpendo will send you a reminder about your upcoming booking. This reminder will include the details of your booking as you have entered them. We suggest you use a **5 day** notice period, so you can review and amend your booking details before they are distributed in the Monday Message. See the process below to [amend a booking](#how-to-amend-or-cancel-your-wip).

You can change the default reminder or notice period in your personal Calpendo settings. To do this, click "Settings" (top right) then "booking reminders" from the list on the left.

##### 3. Description

Please enter "WIP Presentation". This makes it easier for the WIN Wednesday Administrators to review your booking appropriately.

##### 4. Title, Authors and Abstract

Please enter the title of your project, the authors and an abstract. This information will be circulated in the Monday Message in the week of your presentation. **Bookings will not be approved until these details are complete.**

##### 5. Recording consent upload button

A signed speaker recording release ("Recording Consent") is required to record your WIP presentation. Recording allows other WIN members to review and provide feedback on your presentation.

Download and sign a [speaker recording release](https://help.it.ox.ac.uk/files/replaydownloadablelegalpackoct2021zip) from Central IT. A signed release is required for everyone who intends to present in the main part of the meeting. A release is not required for people (for example Supervisors) who only intend to be on screen during the Q&A as this will not be recorded. Choose the MS Word .docx version, or .pdf if MS Word is not available. If using the .pdf, please create text boxes to write over the appropriate white spaces.

Once you have saved your signed release, click the "Choose File" button to locate the file and add it to your booking. **Bookings will not be approved until at least one signed release has been uploaded.**

##### 10. Submit you booking

Click the "Create Booking" button to submit your booking for approval. Your booking will be sent to a WIN Wednesday administrator and reviewed for completeness before it is approved.

You will receive an email confirmation that your booking has been received. This email will be sent to the address associated with your Shibboleth account.

You will receive an email notification once your booking has been approved, or you will be asked for further information. **Your slot will be cancelled by an administrator if the booking information is not complete by 17:00 Friday of the week proceeding your slot.**

### How to amend or cancel your WIP booking

**Bookings can be amended by the booker up to 12:00 on the Friday before your scheduled presentation. If you need to amend your booking after that time, please [contact a WIN Wednesday administrator](#contact).**

To amend your booking, return to the WIN Wednesday resource calendar following steps [2. Log into Calpendo using Shibboleth](#2-log-into-calpendo-using-shibboleth) and [3. Select the WIN Wednesday resource](#3-select-the-win-wednesday-resource) above.

Edit the information as required and click "Update Booking" to save the changes. To cancel your booking, click "Cancel Booking". You will be notified that your booking has been cancelled.

![calpendo edit booking window for WIN Wednesdays with arrows highlighting the update and cancel booking buttons](img/calpendo-amend.png)


## For WIN Wednesday Coordinators

[WIN Wednesday Coordinators](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/WIN%20Wednesday%20Rota.aspx) are the WIN members responsible for convening presentations with content themed around methods, career development, equality, diversity and inclusion, publication round ups, and guest speakers.

Before the start of each academic year, the WIN Wednesday schedule is decided and slots allocated to each of these topics, along with space for special announcements or activities. WIP presentations can be scheduled for the same WIN Wednesday session if time allows. The allocation of WIN Wednesday content will be communicated with the WIN Wednesday Coordinators before the start of the academic year. [Allocations for 2021-2022 are available to the WIN Wednesday Coordinators on Microsoft Teams](https://teams.microsoft.com/l/file/11779894-604e-4db1-9b3e-e6bde563aaaa?tenantId=cc95de1b-97f5-4f93-b4ba-fe68b852cf91&fileType=xlsx&objectUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk%2FDocuments%2FMicrosoft%20Teams%20Chat%20Files%2FWIN%20Wednesday%20Series%202021-2022.xlsx&baseUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk&serviceName=p2p&threadId=19:ecaccdfe326c4351a9bf7ff75f1ca8c4@thread.v2).

### Booking WIN Wednesday seminars

WIN Wednesday Coordinators are required to book their allocated slots in Calpendo using the process described below. Using Calpendo allows for simplified collation of required documentation, automated email reminders and flexibility of scheduling.

#### 1. Create an account

Ensure you have an active Calpendo account. This may take up to one week to issue or reinstate. You can check if your account is active by attempting to [log in using Shibboleth](#2-log-into-calpendo-using-shibboleth) as described below. If you are unable to log in, see [How do I get a Calpendo account](#how-do-i-get-a-calpendo-account).

#### 2. Log into Calpendo using Shibboleth

Go to http://calpendo.fmrib.ox.ac.uk/.

Click the "Shibboleth" button indicated below.

On the following page use your University of Oxford Shibboleth SSO to sign into the Calpendo homepage.

![calpendo sign in page with arrow highlighting Shibboleth sign in button](img/calpendo-shibboleth.png)

#### 3. Select the WIN Wednesday resource

Select the WIN Wednesday resource from the list of available calendars.

Click the "Calendars" tab on the top left of the Calpendo page, then select WIN Wednesdays as shown below.

![calpendo booking calendar with arrows highlighting the calendar drop down and WIN Wednesday resource](img/calpendo-resource.png)

#### 4. Find your allocated slot

Use the week navigation buttons near the top of the page to find to the slot you have been allocated. [Allocations for 2021-2022 are available to WIN Wednesday Coordinators on Microsoft Teams](https://teams.microsoft.com/l/file/11779894-604e-4db1-9b3e-e6bde563aaaa?tenantId=cc95de1b-97f5-4f93-b4ba-fe68b852cf91&fileType=xlsx&objectUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk%2FDocuments%2FMicrosoft%20Teams%20Chat%20Files%2FWIN%20Wednesday%20Series%202021-2022.xlsx&baseUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk&serviceName=p2p&threadId=19:ecaccdfe326c4351a9bf7ff75f1ca8c4@thread.v2).

![calpendo booking calendar for WIN Wednesdays with arrows highlighting the week navigation buttons and an available slot](img/calpendo-available.png)

#### 5. Click to open the "new booking" window

Click your allocated slot in the calendar to open the "new booking" window. Enter the required details as described below.

![calpendo new booking window for a WIN Wednesday seminar with arrows highlighting: 1) end time; 2) email reminder notice period; 3) description text box; 4) Title, Authors and Abstract text box; 5) Recording consent upload button](img/calpendo-newbooking-coord.png)

##### 1. Start and end times

The start time of your booking will depend on whether time is reserved for a WIP presentation. Please refer to the [allocation for 2021-2022](https://teams.microsoft.com/l/file/11779894-604e-4db1-9b3e-e6bde563aaaa?tenantId=cc95de1b-97f5-4f93-b4ba-fe68b852cf91&fileType=xlsx&objectUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk%2FDocuments%2FMicrosoft%20Teams%20Chat%20Files%2FWIN%20Wednesday%20Series%202021-2022.xlsx&baseUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk&serviceName=p2p&threadId=19:ecaccdfe326c4351a9bf7ff75f1ca8c4@thread.v2) to see if there is space for a WIP in the same session as your seminar (note there is space for a WIP unless the row explicitly says "No WIP").

*If there is a WIP in the same session as your seminar, your seminar will last 40 minutes. Please set the start time to 12:20 and the end to 13:00.*

*If there is no WIP in the same session as your seminar, your seminar will last 60 minutes. Please set the start time to 12:00 and the end to 13:00.*

*Please ensure that all speakers are informed of the appropriate duration of their presentation.*

**Please note that if a WIP presentation is booked in that session, it will be held before your seminar. Please ensure, however, that all speakers for your seminar are present for the duration 12:00-13:00 the session.**

**We request that all presenters join the call 10 minutes before the start of the meeting to test audio-visual equipment with the host.**

##### 2. Reminder notice period

This sets the date which Calpendo will send you a reminder about your upcoming booking. This reminder will include the details of your booking as you have entered them. We suggest you use a minimum of a **5 day** notice period, so you can review and amend your booking details before they are distributed in the Monday Message. See the process below to [amend a booking](#how-to-amend-or-cancel-your-win-wednesday-seminar).

You can change the default reminder or notice period in your personal Calpendo settings. To do this, click "Settings" (top right) then "booking reminders" from the list on the left.

##### 3. Description

Please enter name of your seminar series, for example "WIN Wednesday EDI Series". This makes it easier for the WIN Wednesday Administrators to review your booking appropriately.

##### 4. Title, Authors and Abstract

Please enter the title of your seminar, the authors (presenters) and an  abstract. This information will be circulated in the Monday Message in the week of your presentation.

We understand that you may not have details of your Seminar yet. This is not a problem at this stage. We ask, however, that you use the Calpendo or other notice functions to remind you to enter the relevant information into the booking as early as possible, as this is the information which will be used to populate the Monday Message. 

**Bookings will not be approved until these details are complete.**

##### 5. Recording consent upload button

A signed speaker recording release ("Recording Consent") is required to record your WIN Wednesday Seminar. Recording allows other WIN members to review and learn from your seminar. A signed release is required for everyone who intends to present in the main part of the seminar. A release is not required for people who only intend to be on screen during the Q&A as this will not be recorded.

Download a [speaker recording release](https://help.it.ox.ac.uk/files/replaydownloadablelegalpackoct2021zip) from Central IT. **Please distribute the speaker recording release pack to your intended presenters and ask them to return their signed copy to you.** Presenters should sign using the MS Word .docx version, or .pdf if MS Word is not available. If using the .pdf, please create text boxes to write over the appropriate white spaces.

Once returned, please upload the release(s) to your booking. Click the "Choose File" button to locate the release file(s) and add it to your booking. **Bookings will not be approved until at least one signed release has been uploaded.**

*We appreciate that some speakers may be hesitant to have their presentations recorded. Please reassure them that presentation will only be available internally to members of the University of Oxford (SSO is required). Please [contact the WIN Wednesday Administrators](#contact) if there are any concerns.*

##### 10. Submit you booking

Click the "Create Booking" button to submit your booking for approval. Your booking will be sent to a WIN Wednesday administrator and reviewed for completeness before it is approved.

You will receive an email confirmation that your booking has been received. This email will be sent to the address associated with your Shibboleth account.

You will receive an email notification once your booking has been approved, or you will be asked for further information. **You will be contacted by an administrator if the booking information is not complete by 17:00 Friday of the week proceeding your slot.**

### How to amend or cancel your WIN Wednesday seminar

**Booking details (such as names, abstract) can be amended by the booker up to 12:00 on the Friday before the scheduled seminar. If you need to amend your booking after that time, please [contact a WIN Wednesday administrator](#contact).**

To amend your booking, return to the WIN Wednesday resource calendar following steps [2. Log into Calpendo using Shibboleth](#2-log-into-calpendo-using-shibboleth-1) and [3. Select the WIN Wednesday resource](#3-select-the-win-wednesday-resource-1) above.

Edit the information as required and click "Update Booking" to save the changes. To cancel your booking, click "Cancel Booking". You will be notified that your booking has been cancelled.

![calpendo edit booking window for WIN Wednesdays with arrows highlighting the update and cancel booking buttons](img/calpendo-amend-coord.png)

### Approval of your booking

You will get an email from Calpendo when your booking has been approved. Alternatively you may receive an email with an instruction to add or clarify some detail.

Once your booking has been approved you are still able to edit it, but we would prefer if you didn't! Please [contact an administrator](#contact) if you need to make any changes to your booking.

You booking will be cancelled if it is not complete by 17:00 on the Friday preceding your booked slot. You will receive and email notification if your booking is cancelled.

### What if you need to change the date/time of your seminar?

In some circumstances it may be necessary to hold your seminar outside of the usual Wednesday 12:00-13:00 slot, for example to meet the availability of external presenters. In these cases, please [contact a WIN Wednesday Administrators](#contact) and ask them to open a slot in Calpendo at the required time.

## For Administrators

WIN Wednesday administrators are responsible for approving WIP and seminar bookings, and liaising with presenters. They also manage the Calpendo resource and can adjust availability in the template.

Administrators can create bookings any time between 06:00-22:00 Monday-Sunday. All admin bookings will be automatically be approved.

Contact a Calpendo Manager (for example sebastian.rieger@psych.ox.ac.uk) to request that members are added or removed from the WIN Wednesday Administrators Calpendo group.

### Allocate bookings to WIN Wednesday themes

Before the start of the year, allocate slots to each of the speaker themes (e.g. EDI, Educational) as required. Communicate dates with WIN Wednesday Coordinators and ask them to book their slots. Encourage WIN Wednesday Coordinators to enter their bookings on Calpendo before the calendar is made available for WIPs.

Note: It may be wise to communicate annually when "the WIP calendar is now open for bookings", to ensure that all seminars are pre-filled before it goes to wider release.

### Approval of bookings

#### Approval responsibilities

The [WIP coordinator](#contact) (Cassandra Gould van Praag) will review bookings as they are received. If they are complete they will be approved immediately using the process described below. If they are incomplete the booked will be emailed and reminded of the necessary information. The [WIN Wednesday Seminar coordinator](#contact) need not action any Calpendo emails.

On the Friday before the Wednesday, the [WIN Wednesday Seminar coordinator](#contact) will review Calpendo for that week. They will retrieve the required information for the Monday Message directly from Calpendo. Where this information is not complete (i.e. the booking has not already been approved), they will email the booker and inform them that their slot will be cancelled if the information is not promptly received.

#### Approval process

##### 1. Find booking

You will be notified by email when a booking has been made. To review and approve that booking, first find it in Calpendo and click on "view".

![calpendo page for Administrators, with an arrow highlighting the "view" link to review a users booking](img/calpendo-approve-view.png)

##### 2. Review booking

Review the details of the booking to ensure they are ready to be published in the Monday message. Open and review the recording consent. If any details are missing, use the send email" button to contact the user.

![calpendo page for Administrators, showing how to review another users booking. Arrows highlighting the "Title, Authors and Abstract" field, "Recording consent" and "send email" button](img/calpendo-approve-review.png)

##### 3. Approve or deny booking

If all details are present and correct, move on to approve the booking. Click the "Admin" tab at the top of the Calpendo window and select "Booking Requests" to bring up a list of all bookings which require approval.

![calpendo page for Administrators, with arrows highlighting the "Admin" and "Booking Requests" link](img/calpendo-approve-bookinglist.png)

Locate the booking you reviewed. It may be helpful to sort the list of unapproved bookings by Resource. Click the checkbox next to the booking you wish to approve and click "Approve" or deny as appropriate.

![calpendo page for Administrators, with an arrow highlighting the "approve" button on a booking made against the WIN Wednesday resource](img/calpendo-approve-approve.png)

### Communication of the processes

The [WIN Wednesday Seminar coordinator](#contact) will include a link in every Monday Message to the WIP and Calpendo booking guides: "See this information to book your own WIP."

### Zoom links and hosting

The [WIP coordinator](#contact) will be the host of the Zoom call for all seminars where there is a WIP slot, irrespective of whether it is booked. For other WIN Wednesday Seminars where there is no WIP slot, the WIN Wednesday Coordinator will host the zoom call.

The meeting host will be allocated a zoom license by WIN Admin. The host should then schedule the meeting in zoom and provide the joining information to WIN Admin.

### Calpendo testing account

It may be necessary for administrators to have access to a Calpendo account without administrative privileges, so they can test newly implemted features or rules. A local, non-administrator account can be requested by contacting sebastian.rieger@psych.ox.ac.uk.

## Contact

If you have any questions about this process or your booking, please contact one of the WIN Wednesday administrators below:

WIP Coordinator: cassandra.gouldvanpraag@psych.ox.ac.uk
WIN Wednesday Seminar Coordinator: andrew.galloway@ndcn.ox.ac.uk
