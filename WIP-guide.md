# WIN Works in Progress (WIP) Guide

*This guide is intended to be made available for WIN researchers via the intranet.*

**Outline**

1. [What to expect from a WIP Presentation](#1-what-to-expect-from-a-wip-presentation)
2. [Why should I do a WIP presentation?](#2-why-should-i-do-a-wip-presentation)
3. [Who should present and be present at WIP?](#3-who-should-present-and-be-present-at-a-wip)
4. [When should I present a WIP?](#4-when-should-i-present-a-wip)
5. [What should I include in my presentation?](#5-what-should-i-include-in-my-presentation)
6. [How long should my WIP presentation be?](##6-how-long-should-my-wip-presentation-be?)
7. [How do I sign up for a WIP presentation?](#7-how-do-i-sign-up-for-a-wip-presentation)
8. [Who can help me arrange and plan my WIP?](#8-who-can-help-me-arrange-and-plan-my-wip)
9. [Where does the WIP take place?](#9-where-does-the-wip-take-place)
10. [How should I promote my WIP?](#10-how-should-i-promote-my-wip)
11. [What to do after the WIP](#11-what-to-do-after-the-wip)
12. [Useful contacts](#12-useful-contacts)


## Context

The Work in Progress (WIP) meetings provide an informal and constructive forum for researchers to get feedback on planned imaging projects from a diverse WIN audience. WIPs are scheduled as part of the WIN Wednesday meetings that take place on Wednesdays 12:00 - 13:00, everyone who is affiliated with the WIN is welcome to attend these meetings.

The WIN Wednesday weekly meetings are a good opportunity to meet WIN researchers from other groups, find out more about the breadth of research being conducted at WIN, contribute to and learn from the discussion.

We invite you to attend WIPs as often as you can, and be part of these integral "WIN Culture" events. WIP presentations are advertised in the WIN Monday Message (circulated through win-messages@maillist.ox.ac.uk); if you are not currently on this list please email computing-help@win.ox.ac.uk asking to be added.

WIPs are a useful opportunity to get expert feedback on any and all research being conducted at WIN. This includes (but is not limited to):

- new imaging data acquisition
- secondary analysis
- sequence development
- new behavioural interventions
- new software applications or extensions
- new analytic techniques
- training materials
- experimental protocols
- public engagement activities

Note *all new MRI and MEG projects running at WIN are required to give a WIP presentation in order to receive approval and a project code*.

Everyone interested in giving a WIP presentation should try to schedule their presentation as early in their project as possible. *WIPs are meant for early stage work, not for projects nearing completion*.

The below information is provided to guide your preparation for and participation in WIP meetings. Please do not hesitate to contact any of the individuals listed in the [Useful Contacts](#13-useful-contacts) section if you would like to discuss any stage of the process or development of your project.

## 1. What to expect from a WIP Presentation

WIPs are an informal meeting and should be viewed as a positive opportunity for feedback from your peers. WIP attendees are reminded that discussions should be constructive, welcoming, accessible, and at all times in accordance with the [University policies on bullying and harassment](https://edu.admin.ox.ac.uk/university-policy-on-harassment). We are all reminded that you are the expert on your topic, and we are here to support you based on our own experience. As WIP organisers, we welcome your feedback on your experience of planning and presenting your WIP, so we can improve the experience for others,

## 2. Why should I do a WIP presentation?

-   To help optimise the quality of your research with feedback from broad perspectives
-   To encourage you to think early about the reproducibility of your research and opportunities to share your materials.
-   To encourage collaboration among WIN members, by creating opportunities to hear about the work others are doing in all disciplines.

The WIP is not a box ticking exercise, or a test. It is a way to share planned work or work in early stages with your WIN peers. A WIP presentation can provide you with valuable feedback from the amazing WIN community, who are all here to support you in doing the best research you can.

## 3. Who should present and be present at a WIP ?

Any WIN affiliated researcher undertaking a research project. This includes projects relating to:

- Analysis Research
- Physics Research
- Basic Neuroscience Research
- Clinical Research
- Cognitive neuroscience Research

The individual undertaking the majority of the data collection, analysis or content generation (as appropriate for the project being presented) should lead the presentation. In most cases this will be a student or Early Career Researcher.

PIs and Supervisors must be present to support and provide additional information where required. Named collaborators (internal and external) should ideally also be present.

You should also identify any tools (such as specific FSL packages), approaches, behavioural tasks, pulse sequences or datasets you will be using. If you think you will need additional support using these tools, outside that which is available in your research group, please contact the developers and ensure they are invited to your WIP. Please review the [WIN authorship guidelines](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/WIN%20Authorship%20Guide%202021.aspx) for more details on this level of collaboration and the list of [WIN tools, along with tool creators](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/WIN%20Standard%20Acknowledgement%20and%20References.aspx)

## 4. When should I present a WIP?

It is best to give a WIP presentation as **early as possible** in your project. This will enable you to receive and incorporate valuable feedback effectively.

WIPs must be completed minimally 2 weeks before planned data collection for new MRI and MEG projects. You can present ideas for planned projects before data collection, or ethics approvals.

You are welcome to present more than one WIP. For example you could present an idea before collecting pilot data, or accessing shared datasets for analysis projects (e.g. UK Biobank), and then present again after data collection, or analysis has started, but not progressed very far. Any time you give a WIP presentation you should be at a stage where you are willing and able to incorporate feedback from the attendees.

## 5. What should I include in my presentation?

You should present an outline of your intended experiment or analysis, including a brief outline of the motivation, background or justification for the research.

A rough suggested outline of a presentation for new data acquisition is below:

-   List main aim/question of the study. Provide brief background, hypotheses or describe exploratory motivations.
-   List and describe tasks if applicable (Preclinical/Clinical/Cogneuro). Comment on plans to share task materials internally or externally.
-   List and describe analysis methods.
-   List expected results. Describe why they are expected.
-   List protocol details (MRI/MEG/EEG) and comment on any existing or planned uploads to the Open Acquisition database.
-   List tools/software requirements (if needed) and comment on the reproducibility or your analysis pipeline, for example plans to publish your code from the WIN GitLab instance
-   List any datasets you might need access to
-   Describe your plans for [sharing newly acquired data](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/) (imaging or otherwise) and any restrictions which may prevent sharing at this time.

## 6. How long should my WIP presentation be?

WIP presentations are allocated a **10 minute slot: 8 minutes for presentation and 2 minutes for questions.** These times are somewhat flexible, but they will be enforced if more that one WIP presentation is scheduled in a single session. 

## 7. How do I sign up for a WIP presentation?

WIP presentations are booked using Calpendo. See [this guide](calpenodo-booking.md) for how to book your presentation.

At the point of booking your WIP you will be asked to provide the following information:

- Title of the project
- Name of presenter, Principal Investigator and collaborators. Include the Supervisor if the presenter is a student.
- A signed [speaker recording release](https://help.it.ox.ac.uk/files/replaydownloadablelegalpackoct2021zip). The meeting will be recorded during the presentation only and not during the question and answer session. A signed release should be returned for everyone who intends to contribute to the main presentation.

WIPs can be booked up to 10 days before they are scheduled to take place. This ensures there is time to circulate notice of the WIP in the preceding Monday message, and invite guests.

## 8. Who can help me arrange and plan my WIP?

The WIN Core staff named below will be able to assist you in scheduling and promoting your WIP.

- Analysis: taylor.hanayik@ndcn.ox.ac.uk
- Physics: mohamed.tachrount@ndcn.ox.ac.uk or aaron.hess@ndcn.ox.ac.uk
- Preclinical: claire.bratley@ndcn.ox.ac.uk
- Clinical OHBA: clare.odonoghue@psych.ox.ac.uk
- Clinical FMRIB: jessica.walsh@ndcn.ox.ac.uk
- Clinical Neurosciences: marieke.martens@psych.ox.ac.uk
- Cognitive Neuroscience (MRI): sebastian.rieger@psych.ox.ac.uk
- Cognitive Neuroscience (MEG/EEG): anna.camera@psych.ox.ac.uk
- All other themes or questions: cassandra.gouldvanpraag@psych.ox.ac.uk

## 9. Where does the WIP take place?

- WIP meetings take place as part of the WIN Wednesday meetings on **Wednesdays between 12:00 and 13:00**
- Currently meetings take place online. Plans for hybrid and in-person meetings are currently under development.
- For online presentations please be prepared to follow the [WIN policies on the use of Zoom](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/WIN%20Zoom%20Meeting%20Policy.aspx), including recording of your presentation.

## 10. How should I promote my WIP?

Your WIP will be announced in the WIN Monday Message. *We recommend that you also directly contact and invite specific people who you would like feedback from, to maximise your opportunity for constructive review*.

You should consider explicitly inviting people you know who have experience with:

- Closely related experimental designs
- Similar patient populations
- Relevant physics expertise
- Relevant analysis expertise
- Relevant programming or software development expertise

If you're unsure of who to invite, please speak to one of the WIP organisers and they can help advise.

## 11. What if I'm using new resources or tools?



## 12. What to do after the WIP

- Try to incorporate any feedback you were given
- Follow up with main discussion contributors
- Always feel free to ask for more input if needed
- Get a project code if needed (or start the process)

## 13. Useful contacts

### Experimental set-up, task, equipment etc.
If you have any questions about stimulus presentation equipment please get in touch with Seb (sebastian.rieger@psych.ox.ac.uk).

### MRI sequences that you will use
Please arrange to discuss your project with one of the radiographers before your WIP: radiographers@win.ox.ac.uk

If your project has more complex physics requirements please get in touch with Stuart (Stuart.Clare@ndcn.ox.ac.uk).

### Analysis plans
If you have any questions about analysis please contact Taylor (taylor.hanayik@ndcn.ox.ac.uk).

### Reproducibility and output sharing plans ("open science")
The outputs of your project are more than just a paper at the end. They include your experimental protocol, your data, your code and your tasks. We can help you turn these outputs into practical tools for you and your lab to efficiently reuse.

If you have any questions relating to open science practices, including how to use the [Open WIN infrastructure](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/), please contact Cassandra (cassandra.gouldvanpraag@psych.ox.ac.uk).

### Ethics
You do not need to have ethical approval in place before you do your WIP (although it is needed before you can start your study)

If you have any questions about ethics please contact Nancy (Nancy.Rawlings@ndcn.ox.ac.uk).

Pilot data can sometimes be acquired prior to obtaining your own ethical approval by making use of technical development SOPs. Please contact the radiographers to find out more (radiographers@win.ox.ac.uk)

### Core staff support

The WIN Core staff named below are happy to assist you at any stage of planning your WIP.

- Analysis: taylor.hanayik@ndcn.ox.ac.uk
- Physics: mohamed.tachrount@ndcn.ox.ac.uk or aaron.hess@ndcn.ox.ac.uk
- Preclinical: claire.bratley@ndcn.ox.ac.uk
- Clinical OHBA: clare.odonoghue@psych.ox.ac.uk
- Clinical FMRIB: jessica.walsh@ndcn.ox.ac.uk
- Clinical Neuerosciences: marieke.martens@psych.ox.ac.uk
- Cognitive Neuroscience (MRI): sebastian.rieger@psych.ox.ac.uk
- Cognitive Neuroscience (MEG/EEG): anna.camera@psych.ox.ac.uk
- All other themes or questions: cassandra.gouldvanpraag@psych.ox.ac.uk
