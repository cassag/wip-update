# Calpendo automated email workflow

*This file contains the agreed content and triggers for automated emails from calpendo relating to booking of a WIN Wednesday presentation*

**Outline**

- [On booking](#on-booking)
1. - [x] Email to Booker
2. - [x] Email to resource managers
- [Reminder if not approved](#reminder-if-not-approved)
1. - [x] Email to resource managers, Timed -5 days (12:00-13:00 Friday)
- [Reminder if approved](#reminder-if-approved)
1. - [x] Email to Booker, Timed to -6 days (12:00-13:00 Thursday)
2. - [x] Email to Booker, Timed as they requested
- [On cancellation](#on-cancellation)
1. - [x] Email to Booker
2. - [x] Email to resource managers
- [On rejection](#on-rejection)
1. - [x] Email to Booker
2. - [x] Email to resource managers



## On booking
### 1. Email to booker
To: Booker only

Subject: WIN Wednesdays - Your booking for [dd/mm/yyyy] has been sent to Resource Managers for approval


> Dear [Booker]
>
> You have requested a booking for "WIN Wednesdays" on [dd/mm/yyyy].
>
> A resource manager will now review your booking to ensure the required information (title, abstract, authors and signed speaker release) has been supplied. Your booking will not be approved until all details have been entered. Your booking will be cancelled if the information has not been supplied by 17:00 (UK London time) on the Friday preceding your booking.
>
> If you have not already done so, please add the title of the talk, names of the authors, and the abstract to the booking, and upload the signed [speaker recording release](https://help.it.ox.ac.uk/files/replaydownloadablelegalpackoct2021zip). The meeting will be recorded during the presentation only and not during questions. A signed release should therefore be returned for everyone who intends to contribute to the main presentation.
>
> Please see [this guide](https://git.fmrib.ox.ac.uk/cassag/wip-update/-/blob/master/WIP-guide.md) for a description of what your presentation should cover.
>
> Please note that the exact start time of each talk within a session is subject to change, and therefore speakers are required to be present for the duration of the session.
>
> If you have any questions, feel free to contact the WIN Wednesdays resource managers: andrew.galloway@ndcn.ox.ac.uk; cassandra.gouldvanpraag@psych.ox.ac.uk
>
> Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored.


### 2. Email to Resource manager
To: Resource manager group

Subject: WIN Wednesdays - New Booking has been created by [Booker] on [dd/mm/yyyy]

> Dear WIN Wednesdays resource managers,
>
> A new booking has been created on WIN Wednesdays by Shibboleth/psyc1182@ox.ac.uk for 22/09/2021.
>
> Please review and approve this booking here: [Link to approval]
>
> Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored.


## Reminder if not approved
### 1. Timed -5 days (12:00 Friday)
To: Resource managers group
Subject: WIN Wednesdays - Review booking today


> Dear Resource Managers,
>
> A WIN Wednesday booking for [dd/mm/yyyy] requires your attention. Please approve or cancel this booking here by 17:00 today: [link to approve]
>
> Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored.


## Reminder if approved
### 1. Timed to -6 days (12:00-13:00 Thursday)
Incase they don't set one. Nice to have one to remind that detail will be sent out in the Monday Message. Can this email include detail from the booking?

To: Booker only

Subject: WIN Wednesdays - Your booking reminder for [dd/mm/yyyy]


> Dear [Booker]
>
> This is a reminder for your scheduled WIN Wednesday presentation on [dd/mm/yyyy] at 12:00.
>
> The details below will be circulated in the next Monday Message. Please ensure they are correct. Amendments can be made until 17:00 Friday.
>
> [Description]
>
> [Title, Abstract, Authors]
>
> Please see [this guide](https://git.fmrib.ox.ac.uk/cassag/wip-update/-/blob/master/WIP-guide.md) for a description of what your presentation should cover.
>
> If you have any questions, feel free to contact the WIN Wednesdays resource managers: andrew.galloway@ndcn.ox.ac.uk; cassandra.gouldvanpraag@psych.ox.ac.uk
>
> Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored.


### 2. Timed as booker requested
To: Booker only
Subject: WIN Wednesdays - Your booking reminder for [dd/mm/yyyy]

[As above]

## On cancellation
### 1. Email to booker
To: Booker only

Subject: WIN Wednesdays - Your booking for [dd/mm/yyyy] has been cancelled


> Dear [Booker]
>
> Your booking for "WIN Wednesdays" on [dd/mm/yyyy] has been cancelled.
>
> Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored.


### 2. Email to Resource managers
To: Resource managers group

Subject: WIN Wednesdays - The booking has been cancelled for [Booker] on [dd/mm/yyyy]


> Dear Resource managers
>
> The booking for "WIN Wednesdays" on [dd/mm/yyyy] by [Booker] has been cancelled.
>
> Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored.



## On rejection
### 1. Email to booker
To: Booker only

Subject: WIN Wednesdays - Your booking for [dd/mm/yyyy] has been rejected


> Dear [Booker]
>
> Your booking for "WIN Wednesdays" on [dd/mm/yyyy] has been rejcted by the resource manager.
>
> The booking has been rejected either because the required information (title, abstract, authors) was not sufficiently complete for distribution, or the speaker recording release was not signed and uploaded on time.
>
> If you have any questions, feel free to contact the WIN Wednesdays resource managers: andrew.galloway@ndcn.ox.ac.uk; cassandra.gouldvanpraag@psych.ox.ac.uk.
>
> Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored.



### 2. Email to Resource managers
***Can we add notes when we reject a booking?***

To: Resource managers group

Subject: WIN Wednesdays - You have rejected a booking for [dd/mm/yyyy]


> Dear Resource managers
>
> The booking for "WIN Wednesdays" on [dd/mm/yyyy] by [Booker] has been rejected.
> You gave the reason [reason]. Please contact [Booker] to discuss.
>
> Sent by the WIN Booking System. Please do not reply to this email as this address is not monitored.
